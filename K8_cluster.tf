provider "google" {
    credentials = "${file(var.cred_file)}"
    project = var.project_id
    region = var.location_name
}
resource "google_container_cluster" "test_cluster" {
    name = var.cluster_name
    location = var.location_name
    remove_default_node_pool = true
    initial_node_count = 1
}
resource "google_container_node_pool" "custom_nodes" {
    name = "custom-nodes"
    location = var.location_name
    cluster = google_container_cluster.test_cluster.name
    node_count = 1
    node_config {
      machine_type = "e2-micro"
    }
    depends_on = [
      google_container_cluster.test_cluster
    ]
}
