variable "project_id" {
  type = string
  default = "k8s-practice-326211"
}
variable "cred_file" {
  type = string
  default = "k8s-practice-326211-198d1f19028d.json"
}
variable "cluster_name" {
    type = string
    default = "ci-cd-cluster2"
}
variable "location_name" {
    type = string
    default = "us-east1"
}
